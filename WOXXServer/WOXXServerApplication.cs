﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Photon.SocketServer;
using Photon.SocketServer.Diagnostics;
using log4net.Config;
using ExitGames.Logging;
using ExitGames.Logging.Log4Net;

namespace WOXXServer
{
    using WOXXServer.Diagnostics;

    public class WOXXServerApplication : ApplicationBase
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        protected override PeerBase CreatePeer(InitRequest initRequest)
        {
            // 建立连接并回传给PhotonServer
            return new WOXXServerPeer(initRequest.Protocol, initRequest.PhotonPeer);
        }

        protected override void Setup()
        {
            // 初始化GameServer
            log4net.GlobalContext.Properties["Photon:ApplicationLogPath"] = Path.Combine(this.ApplicationRootPath, "log");

            // log4net
            string path = Path.Combine(this.BinaryPath, "log4net.config");
            var file = new FileInfo(path);
            if (file.Exists)
            {
                LogManager.SetLoggerFactory(Log4NetLoggerFactory.Instance);
                XmlConfigurator.ConfigureAndWatch(file);
            }

            log.InfoFormat("Created WOXXServer Instance: type={0}", Instance.GetType());

            // counters for the photon dashboard
            CounterPublisher.DefaultInstance.AddStaticCounterClass(typeof(Counter), "WOXXServer");
            Protocol.AllowRawCustomValues = true;
        }

        protected override void TearDown()
        {
            // 关闭GameServer并释放资源
        }
    }
}
