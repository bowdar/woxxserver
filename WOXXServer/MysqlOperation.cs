﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using MySql.Data.MySqlClient;

public abstract class MySqlUtil
{

    public static string strConn = "datasource=127.0.0.1;username=root;password=root;database=woxx;charset=gbk";

    private static void PrepareCommand(MySqlCommand cmd, MySqlConnection conn, string cmdText, MySqlParameter[] cmdParms)
    {
        if (conn.State != ConnectionState.Open)
            conn.Open();
        cmd.Connection = conn;
        cmd.CommandText = cmdText;
        if (cmdParms != null)
        {
            foreach (MySqlParameter parm in cmdParms)
                cmd.Parameters.Add(parm);
        }
    }

    public static int ExecuteNonQuery(string cmdText, MySqlParameter[] cmdParms)
    {
        MySqlCommand cmd = new MySqlCommand();
        using (MySqlConnection conn = new MySqlConnection(strConn))
        {
            PrepareCommand(cmd, conn, cmdText, cmdParms);
            int val = cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            conn.Close();
            return val;
        }
    }

    public static object ExecuteScalar(string cmdText, MySqlParameter[] cmdParms)
    {
        MySqlCommand cmd = new MySqlCommand();
        using (MySqlConnection conn = new MySqlConnection(strConn))
        {
            PrepareCommand(cmd, conn, cmdText, cmdParms);
            object obj = cmd.ExecuteScalar();
            cmd.Parameters.Clear();
            conn.Close();
            return obj;
        }
    }

    public static DataSet GetDataSet(string cmdText, MySqlParameter[] cmdParms)
    {
        MySqlCommand cmd = new MySqlCommand();
        using (MySqlConnection conn = new MySqlConnection(strConn))
        {
            PrepareCommand(cmd, conn, cmdText, cmdParms);
            MySqlDataAdapter adpt = new MySqlDataAdapter();
            adpt.SelectCommand = cmd;
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            cmd.Parameters.Clear();
            conn.Close();
            return ds;
        }
    }

    public static DataTable GetDataTable(string cmdText, MySqlParameter[] cmdParms)
    {
        MySqlCommand cmd = new MySqlCommand();
        using (MySqlConnection conn = new MySqlConnection(strConn))
        {
            PrepareCommand(cmd, conn, cmdText, cmdParms);
            MySqlDataAdapter adpt = new MySqlDataAdapter();
            adpt.SelectCommand = cmd;
            DataSet ds = new DataSet();
            adpt.Fill(ds);
            cmd.Parameters.Clear();
            conn.Close();
            return ds.Tables[0];
        }
    }

    public static MySqlDataReader GetDataReader(string cmdText, MySqlParameter[] cmdParms)
    {
        MySqlCommand cmd = new MySqlCommand();
        MySqlConnection conn = new MySqlConnection(strConn);
        try
        {
            PrepareCommand(cmd, conn, cmdText, cmdParms);
            MySqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            cmd.Parameters.Clear();
            return reader;
        }
        catch
        {
            conn.Close();
            throw;
        }
    }

}