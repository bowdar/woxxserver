﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Photon.SocketServer;
using PhotonHostRuntimeInterfaces;
using ExitGames.Logging;
using WOXXServer.Common;

namespace WOXXServer
{
    public class WOXXServerPeer : PeerBase
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        #region 构造与析构

        public WOXXServerPeer(IRpcProtocol rpcProtocol, IPhotonPeer nativePeer)
                            : base(rpcProtocol, nativePeer)
        {
        }

        #endregion

        protected override void OnDisconnect(PhotonHostRuntimeInterfaces.DisconnectReason reasonCode, string reasonDetail)
        {
            //TODO : 失去连接时需要做的事
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters)
        {
            //TODO : 取得Client端传过来的要求并加以处理
            switch ((OperationCode)operationRequest.OperationCode)
            {
                case OperationCode.Login:
                {
                    OperationRequestLogin(operationRequest, sendParameters);
                    break;
                }
                case OperationCode.EnterWorld:
                {
                    OperationRequestEnterWorld(operationRequest, sendParameters);
                    break;
                }
            }
        }

        protected void OperationRequestLogin(OperationRequest operationRequest, SendParameters sendParameters)
        {
            if (operationRequest.Parameters.Count < 2) //若参数小于2则返回错误
            {
                //返回登陆错误
                OperationResponse response = new OperationResponse(operationRequest.OperationCode)
                {
                    ReturnCode = (short)2,
                    DebugMessage = "Login Fail"
                };
                SendOperationResponse(response, new SendParameters());
            }
            else
            {
                var loginID = (string)operationRequest.Parameters[1];
                var loginPW = (string)operationRequest.Parameters[2];

                log.InfoFormat("WOXXClient发来登录请求: peerId={0} userName={1}", this.ConnectionId, loginID);
                DataTable dtUser = MySqlUtil.GetDataTable("SELECT * FROM woxx.user WHERE username='" + loginID + "'", null);
                if (dtUser.Rows.Count > 0)
                {
                    string playerPW = dtUser.Rows[0]["password"].ToString();
                    int nUserID = Convert.ToInt32(dtUser.Rows[0]["id"]);

                    if (loginPW == playerPW)
                    {
                        DataTable dtCharacterList = MySqlUtil.GetDataTable("SELECT id,name FROM woxx.character WHERE user_id='" + nUserID + "'", null);
                        var parameter = new Dictionary<byte, object> { };
                        foreach (DataRow row in dtCharacterList.Rows)
                        {
                            parameter.Add(Convert.ToByte(row["id"]), row["name"].ToString());
                        }
                        OperationResponse response = new OperationResponse(operationRequest.OperationCode,
                                                                            parameter
                                                                            ) { ReturnCode = (short)0, DebugMessage = "" };
                        SendOperationResponse(response, new SendParameters());
                    }
                    else
                    {
                        OperationResponse response = new OperationResponse(
                                                        operationRequest.OperationCode)
                        {
                            ReturnCode = (short)1,
                            DebugMessage = "Wrong password"
                        };
                        SendOperationResponse(response, new SendParameters());
                    }
                }
                else
                {
                    OperationResponse response = new OperationResponse(
                                                    operationRequest.OperationCode)
                    {
                        ReturnCode = (short)1,
                        DebugMessage = "Wrong id"
                    };
                    SendOperationResponse(response, new SendParameters());
                }
            }
        }

        protected void OperationRequestEnterWorld(OperationRequest operationRequest, SendParameters sendParameters)
        {
            if(operationRequest.Parameters.Count < 1) return;
            string strCharacterName = (string)operationRequest.Parameters[1];
            log.InfoFormat("{0}请求进入游戏", strCharacterName);

            DataTable dtCharacter = MySqlUtil.GetDataTable("SELECT * FROM woxx.character WHERE name='" + strCharacterName + "'", null);
            if (dtCharacter.Rows.Count != 1)
            {
                log.WarnFormat("{0}用户非法！", strCharacterName);
                return;
            }
            var parameter = new Dictionary<byte, object> { {1, dtCharacter.Rows[0]["position_x"]},
                                                           {2, dtCharacter.Rows[0]["position_y"]},
                                                           {2, dtCharacter.Rows[0]["position_z"]},
                                                         };
            OperationResponse response = new OperationResponse(operationRequest.OperationCode,
                                                                            parameter
                                                                            ) { ReturnCode = (short)0, DebugMessage = "" };
            SendOperationResponse(response, new SendParameters());
        }
    }
}
