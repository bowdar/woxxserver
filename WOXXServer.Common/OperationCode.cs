﻿namespace WOXXServer.Common
{
    public enum OperationCode : byte
    {
        Nil = 0,
        Login = 5,
        EnterWorld = 6,
        ExitWorld = 7
    }
}
